import elasticsearch
from flask import Flask, request, jsonify
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import json


app = Flask(__name__)

@app.route("/")
def home():
    return "TEST"

@app.route("/load", methods=['POST'])
def load():
    if request.method == 'POST':

        es = Elasticsearch(
            "http://elasticsearch:9200",
            basic_auth=("root", "root"))
        # print('###########')
        # print(request.get_json())
        data = request.json
        # print('###########')

        resp = es.info()
        print(resp)
        resp = es.index(index="d_courses", id=1, document={"blabla": "bloblo"})

        # for key in data:
        #     es.index(index="d_courses", id=key, document=data[key])


        for key in data:
            resp = es.index(index="d_courses", id=key, document=data[key])

            #actions = [
             #   {
            #        '_index': "d_courses",
             #       '_type': 'text',
              #      '_id': key,
               #     '_source': data[key],
                #}
            #]
        #es.bulk(body=actions) 

        response = es.search(index="d_courses", query={"match_all": {} })
        
        return str(response["hits"]["hits"])

    else:
        return "protocole not supported"
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=10003, debug=True)