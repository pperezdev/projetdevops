from time import time
from flask import Flask, jsonify
import mysql.connector
import requests
import time

app = Flask(__name__)

@app.route("/run")
def run():
    conn = mysql.connector.connect(user='root', password='root',
                        host='db',
                        database='devops',
                        port="3306")
    cursor =conn.cursor()

    cursor.execute("SELECT * from D_COURSES")
    result = cursor.fetchall()
    json_result = {}

    for data in result:
        json_result[data[0]] = { 
            "id_courses": data[0],
            "name": data[1],
            "description": data[2],
            "number_of_hour": data[3],
            "teacher_name": data[4],
            "date_insert": data[5],
            "date_update": data[6],
            "delete_line": data[7]
        }
    conn.close()
    requests.post("http://app2:5000/load", data=json_result)
    return json_result




if __name__ == '__main__':
    app.run(host='0.0.0.0', port=10002, debug=True)